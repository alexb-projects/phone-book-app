package handlers

import (
    "encoding/json"
    "log"
    "net/http"
)

func HealthHandler() http.HandlerFunc {
    return func(w http.ResponseWriter, r *http.Request) {
        log.Printf("Received request: %s %s", r.Method, r.URL.Path)

        if r.Method != http.MethodGet {
            log.Printf("Method not allowed: %s %s", r.Method, r.URL.Path)
            http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
            return
        }

        // Define a fixed JSON response
        healthResponse := map[string]string{"status": "ok"}

        // Serialize the response to JSON and write the response
        jsonBytes, err := json.Marshal(healthResponse)
        if err != nil {
            log.Printf("Error marshaling JSON: %v", err)
            http.Error(w, "Internal Server Error", http.StatusInternalServerError)
            return
        }

        w.Header().Set("Content-Type", "application/json")
        w.WriteHeader(http.StatusOK)
        w.Write(jsonBytes)
    }
}
