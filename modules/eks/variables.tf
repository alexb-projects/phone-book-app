variable "region" {
  description = "AWS region. Ex.: us-east-1"
  type        = string
  default     = "us-east-1"
}
variable "vpc_cidr" {
  type        = string
  description = "VPC CIDR range"
}

variable "public_subnet_cidrs" {
  type        = list(string)
  description = "Public subnets CIDRs"
}

variable "org_name" {
  type        = string
  description = "Organization name"
}

variable "tags" {
  type        = map(string)
  description = "tags"
}

variable "nodegroup_desired_size" {
  type        = number
  description = "Desired amount of nodes in the cluster"
  default     = 1
}

variable "nodegroup_max_size" {
  type        = number
  description = "Maximum amount of nodes in the cluster"
  default     = 1
}

variable "nodegroup_min_size" {
  type        = number
  description = "Minimum amount of nodes in the cluster"
  default     = 1
}
