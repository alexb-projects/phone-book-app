package handlers

import (
    "database/sql"
    "encoding/json"
    "log"
    "net/http"
)

func SearchContactsHandler(db *sql.DB) http.HandlerFunc {
    return func(w http.ResponseWriter, r *http.Request) {
        log.Printf("Received request: %s %s", r.Method, r.URL.Path)
        
        // Handle preflight OPTIONS request
        if r.Method == http.MethodOptions {
            w.Header().Set("Access-Control-Allow-Origin", "*")
            w.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS")
            w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
            w.WriteHeader(http.StatusOK)
            return
        }
        if r.Method != http.MethodPost {
            log.Printf("Method not allowed: %s %s", r.Method, r.URL.Path)
            http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
            return
        }

        // Parse the JSON data from the request
        var searchParams struct {
            Name        string `json:"name"`
            PhoneNumber string `json:"phoneNumber"`
        }

        if err := json.NewDecoder(r.Body).Decode(&searchParams); err != nil {
            log.Printf("Error parsing request body: %v", err)
            http.Error(w, err.Error(), http.StatusBadRequest)
            return
        }

        // Query the database to fetch matching contacts
        query := "SELECT name, phone_number FROM phone_book WHERE name LIKE $1 AND phone_number LIKE $2"
        rows, err := db.Query(query, "%"+searchParams.Name+"%", "%"+searchParams.PhoneNumber+"%")
        if err != nil {
            log.Printf("Error querying the database: %v", err)
            http.Error(w, "Internal Server Error", http.StatusInternalServerError)
            return
        }
        defer rows.Close()

        // Create a slice to store the matching contacts
        var contacts []map[string]string

        // Iterate through the rows and add each matching contact to the slice
        for rows.Next() {
            var name, phoneNumber string
            if err := rows.Scan(&name, &phoneNumber); err != nil {
                log.Printf("Error scanning row: %v", err)
                http.Error(w, "Internal Server Error", http.StatusInternalServerError)
                return
            }
            contact := map[string]string{"name": name, "phone_number": phoneNumber}
            contacts = append(contacts, contact)
        }

        // Check for errors from iterating over rows
        if err := rows.Err(); err != nil {
            log.Printf("Error iterating over rows: %v", err)
            http.Error(w, "Internal Server Error", http.StatusInternalServerError)
            return
        }

        // Serialize the matching contacts to JSON and write the response
        jsonBytes, err := json.Marshal(contacts)
        if err != nil {
            log.Printf("Error marshaling JSON: %v", err)
            http.Error(w, "Internal Server Error", http.StatusInternalServerError)
            return
        }
        w.Header().Set("Access-Control-Allow-Origin", "*")
        w.Header().Set("Content-Type", "application/json")
        w.WriteHeader(http.StatusOK)
        w.Write(jsonBytes)
    }
}
