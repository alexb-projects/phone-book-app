package main

import (
    "log"
    "net/http"
    "os"
    "backend/src/db"
    "backend/src/handlers"
)

func main() {
    logFile, err := os.Create("service.log")
    if err != nil {
        log.Fatalf("Error creating log file: %v", err)
    }
    defer logFile.Close()

    // log.SetOutput(logFile)
    log.Println("Service started")

    db, err := database.InitializeDatabase()
    if err != nil {
        log.Fatalf("Error initializing database: %v", err)
    }
    defer db.Close()

    // Call CreateTableIfNotExists to create the table
    database.CreateTableIfNotExists(db)

    handlers.SetupRoutes(db)

    port := "8080"
    log.Printf("Server is running on :%s", port)
    log.Fatal(http.ListenAndServe(":"+port, nil))
}
