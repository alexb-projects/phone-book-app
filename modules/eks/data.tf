data "aws_availability_zones" "available" {}

data "aws_iam_policy_document" "cluster_role" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = ["eks.amazonaws.com", ]
    }
  }
}

data "aws_iam_policy_document" "node_role" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com", ]
    }
  }
}